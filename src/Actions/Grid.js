import jsonpFallback  from 'jsonp-fallback';

let gridConfig={'filters':[]};


export const getGridConfig = (model) => {

    gridConfig.model=model;
    return function (dispatch) {
        let url=['http://reczniki.dev:8080/app_dev.php/admin/api',model,'grid'].join(__dirname);

        jsonpFallback(url, {})
            .then(data => {
                dispatch({type: 'RECEIVE_GRID_CONFIG', payload: data});
            })
            .catch(console.error);

        url=['http://reczniki.dev:8080/app_dev.php/admin/api',model].join(__dirname);
        jsonpFallback(url, {})
            .then(data => {
                dispatch({type: 'RECEIVE_GRID_DATA', payload: data});
            })
            .catch(console.error);
    }
};

export const gridAddFilter=(name, value) => {
    gridConfig.filters[name]=value;
    gridConfig.page=1;

    return changeGrid();
}

export const changeGrid = (params) => {
    gridConfig=Object.assign({}, gridConfig, params);


     return function (dispatch) {
         dispatch({type: 'RELOADING', payload:{state: 'processing'}});

         let url=['http://reczniki.dev:8080/app_dev.php/admin/api',gridConfig.model].join(__dirname);

         jsonpFallback(url, {page:gridConfig.page, filters:gridConfig.filters})
             .then(data => {
                 dispatch({type: 'RECEIVE_GRID_DATA', payload: data});

             })
             .catch(console.error);
     }
};