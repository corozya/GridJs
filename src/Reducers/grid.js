
let defaultState = {columns:[],data:[], page:0, pages:0, results:0};


export default function (state = defaultState, action) {

    switch (action.type) {
        case 'RELOADING':
            return Object.assign({}, state, action.payload);
        case 'RECEIVE_GRID_CONFIG':
            return Object.assign({}, state, action.payload);
        case 'RECEIVE_GRID_DATA':
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }

}