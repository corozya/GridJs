import React from 'react';
import { connect } from 'react-redux';

require ('./styles/grid.css');


class Grid extends React.Component {

    constructor(props){
        super();
        this.state={
            model: props.model,
        };
    }

    render() {
        const className=['grid', this.props.type].join(' ');
        return (
            <table className={className} >
                {this.props.children}
            </table>
        );
    }
}

const mapStateToProps = (state) => {
    return {type:state.type,};
};

export default connect(mapStateToProps,null)(Grid);