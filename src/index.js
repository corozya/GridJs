import React from 'react';
import ReactDOM from 'react-dom';
import Wrapper from './Wrapper';
import { createStore, applyMiddleware  } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import GridReducers from './Reducers/grid';
import promise from "redux-promise-middleware";
import { createLogger } from 'redux-logger'
import registerServiceWorker from './registerServiceWorker';



const renderGrid=(name, dest) => {
    const defaultState={
        columns:[],
        data:[],
        page:0, pages:0, results:0,
    };

    const store = createStore(GridReducers,defaultState, applyMiddleware(createLogger(),promise(), thunk));
    ReactDOM.render(
        <Provider store={store}><Wrapper model={name}/></Provider>,
        dest
    );
}

document.renderGrid=renderGrid;
registerServiceWorker();