import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';

import {getGridConfig} from './Actions/Grid';
import Grid from './Grid';
import Filters from './Grid/Filters';
import Header from './Grid/Header';
import Content from './Grid/Content';
import Navigation from './Grid/Navigation';
require ('./styles/grid.css');


class Wrapper extends React.Component {

    constructor(props){
        super();
        this.state={
            model: props.model,
        };
    }

    render() {

        let components=[
            <Header/>,
            <Content/>,
            <Navigation/>
        ];

        if(this.props.type=='gallery'){
            components=[
                <Content/>,
                <Navigation/>
            ];
        }
        return (
            <div>
                <Filters/>
                <Grid params={this.props.grid}>
                    {components}
                </Grid>
            </div>
        );
    }

    componentWillMount(){
        this.props.getGridConfig(this.state.model);
    }
}

const mapStateToProps = (state) => {
    return {
        grid:state.grid,
        type:state.type,

    };
};


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getGridConfig: getGridConfig,
    }, dispatch);
};

export default connect(mapStateToProps,mapDispatchToProps)(Wrapper);