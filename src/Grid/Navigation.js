import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';

import {changeGrid} from '../Actions/Grid';

class Navigation extends React.Component {


    constructor(props){
        super();
    }

    render() {
        return (
            <tfoot>
                <div className="info">{this.props.meta.results}</div>
                <div className="navigation">{this.renderLinks()}</div>
            </tfoot>
        );
    }

    goToPage(page){
        if(page!==this.props.meta.page) {
            this.props.changeGrid({page: page});
        }
    }

    renderLinks(){
        if(this.props.meta.pages===1){
            return false;
        }
        let links=[];

        if(this.props.meta.page>1 && this.props.meta.pages>0){
            links.push(<span onClick={()=>this.goToPage(this.props.meta.page-1)} key='prev'>poprzedni</span>);
        }

        for(let i=1; i<=Math.min(this.props.meta.pages,15); i++){
            links.push(<span onClick={()=>this.goToPage(i)} key={i} className={[(i===this.props.meta.page ? "active" : null)].join(' ')}>{i}</span>);
        }

        if(this.props.meta.page<this.props.meta.pages){
            links.push(<span onClick={()=>this.goToPage(this.props.meta.page+1)} key='next'>następny</span>);
        }
        return links;
    }
}

Navigation.defaultProps={
    meta: { page: 0, pages:0  },
};

const mapStateToProps = (state) => {
    return {
        meta:state.meta,
    };
};


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        changeGrid: changeGrid,
    }, dispatch);
};

export default connect(mapStateToProps,mapDispatchToProps)(Navigation);