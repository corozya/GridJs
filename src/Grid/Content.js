import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';

import {getGridConfig} from '../Actions/Grid';

class Content extends React.Component {

    constructor(props){
        super();
    }

    render() {
        return (
            <tbody>
                {this.renderRows()}
            </tbody>
        );
    }

    renderRows(){
        return this.props.data.map((row,index)=>{
            return (
                <tr key={index} className={index%2 ? 'even' : 'odd'}>{this.renderColumns(row)}</tr>
        );


        })
    }

    renderColumns(row){
        return this.props.columns.map((column,index)=>{
            let data='';
            switch (column.type){
                case 'image':
                case 'thumbnail':
                    //const img=['http://reczniki.dev:8080/app_dev.php/cache/250x250',row[column.name]].join(__dirname);
                    column.description.width=column.description.width ? column.description.width : 150;
                    column.description.height=column.description.height ? column.description.height : 120;
                    const img=['http://reczniki.dev:8080/app_dev.php/cache/', column.description.width ,'x',column.description.height,'/',row[column.name]].join('');

                    return (<td key={index} className={column.type}><img src={img} alt=""/></td>);
                case 'links':
                    return (<td key={index} className='picture'><img src={img} alt=""/></td>);
                case 'bool':
                    data=(<span className={['bool'].join(' ')} />);
                    break;
                default:
                    data=row[column.name];
            }

            return (
                <td key={index}>{data}</td>);
        })
    }
}

const mapStateToProps = (state) => {
    return {
        columns:state.columns,
        data:state.data,
    };
};


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getGridConfig: getGridConfig,
    }, dispatch);
};

export default connect(mapStateToProps,mapDispatchToProps)(Content);