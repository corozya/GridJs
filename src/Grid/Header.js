import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';

import {getGridConfig} from '../Actions/Grid';

class Header extends React.Component {

    constructor(props){
        super();
    }

    render() {
        return (
            <thead>
                <tr>
                {this.renderColumns()}
                </tr>
            </thead>
        );
    }

    renderColumns(){

        return this.props.columns.map((column,index)=>{
            return (
                <td key={index} className={column.type}>{column.description.header}</td>);
        })
    }
}

const mapStateToProps = (state) => {
    return {columns:state.columns};
};


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getGridConfig: getGridConfig,
    }, dispatch);
};

export default connect(mapStateToProps,mapDispatchToProps)(Header);