import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';

import Choices from './Filters/Choices';
import Filter from './Filters/Filter';

import {getGridConfig} from '../Actions/Grid';

class Filters extends React.Component {


    constructor(props){
        super();
    }

    render() {
        return (
            <section className="filters">
                <form>
                    {this.renderFilters()}
                </form>
            </section>
        );
    }

    renderFilters(){
        return this.props.filters.map((filter, index)=>{
            switch(filter.type){
                case 'ChooseType':
                    return (<Choices key={index} config={filter}/>)
                default:
                    return (<Filter key={index} config={filter}/>)
            }

        })
    }
}

Filters.defaultProps={
    filters: [],
};

const mapStateToProps = (state) => {
    return {filters:state.filters};
};


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getGridConfig: getGridConfig,
    }, dispatch);
};

export default connect(mapStateToProps,mapDispatchToProps)(Filters);