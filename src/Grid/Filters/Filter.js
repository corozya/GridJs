import React from 'react';

class Filter extends React.Component {


    constructor(props){
        super();
    }

    handleClick(value){
        alert(value);
    }

    render() {
        return (
            <div className="filter">
                <label>{this.props.config.label}</label>
                <div className="items"><input type="text" name={this.props.config.name}/></div>
            </div>
        );
    }

    renderFilters(){
        // return this.props.config.choices.map((choice, index)=>{
        //     return (
        //         <span key={index} onClick={this.handleClick.bind(this,choice.value)}>{choice.label}</span>
        //             );
        // })
    }
}

Filter.defaultProps={
    config: {},
};

export default Filter;