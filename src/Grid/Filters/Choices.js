import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import Filter from './Filter';
import {changeGrid, gridAddFilter} from '../../Actions/Grid';

class Choices extends Filter {


    constructor(newprops){
        super();

        this.state={
            choices: newprops.config.choices,
        }
    }

    componentWillReceiveProps(newProps){
        if(newProps.choices){
            this.setState({choices:newProps.choices});
        }
    }

    handleClick(value, active){
        const newChoices=this.state.choices.map((choice)=>{
            if(choice.id===value){
                choice.active=active;
            }
            return choice;
        })

        this.setState({choices:newChoices});

        const activeChoices=[];

        this.state.choices.map((choice)=>{
            if(choice.active===true){
                activeChoices.push(choice.id);
            }
        });

        this.props.gridAddFilter(this.props.config.name, activeChoices);
    }

    render() {
        return (
            <div className="filter choices">
                <label>{this.props.config.label}</label>
                <div className="items">{this.renderFilters()}</div>
            </div>
        );
    }

    renderFilters(){
        return this.props.config.choices.map((choice, index)=>{
            return (
                <span key={index}
                      className={choice.active ? 'active' : null}
                      onClick={this.handleClick.bind(this,choice.id, !choice.active)}>{choice.name}</span>
                    );
        })
    }
}

Choices.defaultProps={
    config: {},
};

const mapStateToProps = (state) => {
    return {};
};


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        gridAddFilter: gridAddFilter,
        changeGrid: changeGrid,
    }, dispatch);
};

export default connect(mapStateToProps,mapDispatchToProps)(Choices);